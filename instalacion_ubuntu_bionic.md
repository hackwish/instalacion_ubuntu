# INSTALAR UBUNTU 18.04 - MINT 19 Y DERIVADOS #

## post instalaciÃ³n ##
``sudo apt update``
``sudo apt upgrade``
``sudo apt dist-upgrade``

#ADD REPOS

sudo add-apt-repository ppa:numix/ppa
sudo add-apt-repository ppa:system76/pop
sudo add-apt-repository ppa:system76-dev/stable
sudo add-apt-repository ppa:git-core/ppa

sudo apt install curl wget vim apt-transport-https ca-certificates curl software-properties-common

#SUBLIME TEXT 3
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt-add-repository "deb https://download.sublimetext.com/ apt/stable/"

#SYNCTHING
# Add the release PGP keys:
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
# Add the "stable" channel to your APT sources:
echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list

#DOCKER
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable edge"

#NODEJS
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

#GIT-LFS
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
**en caso de linux mint cambiar linuxmint tara, por ubuntu bionic**
/etc/apt/sources.list.d/github_git-lfs.list

#Google Cloud SDK
## Create environment variable for correct distribution
export CLOUD_SDK_REPO="cloud-sdk-bionic"

## Add the Cloud SDK distribution URI as a package source
echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

## Import the Google Cloud Platform public key
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -


1. actualizar
sudo apt update
sudo apt upgrade
sudo apt dist-upgrade

apt-cache policy docker-ce

sudo apt install ubuntu-restricted-extras 

sudo apt install linux-headers-$(uname -r) build-essential checkinstall make automake cmake autoconf git libdvd-pkg  intel-microcode synaptic apt-xapian-index gdebi preload file-roller p7zip-full p7zip-rar rar unrar zip unzip unace p7zip-full p7zip-rar sharutils mpack arj brasero-cdrkit k3b vlc browser-plugin-vlc soundconverter  catfish hardinfo gufw ttf-mscorefonts-installer libncurses5-dev build-essential module-assistant lm-sensors hddtemp pepperflashplugin-nonfree icedtea-plugin ttf-mscorefonts-installer ttf-bitstream-vera ttf-dejavu terminator htop dconf-editor fonts-noto lshw nmap curl xclip mailutils postfix meld zsh rar unace p7zip-full p7zip-rar sharutils mpack arj htop atop nmon postfix lshw tcpdump ncdu multitail ccze nano iftop stress snmp snmp-mibs-downloader snmpd pv iotop smartmontools python python-setuptools nmap powertop collectd default-jre default-jdk openssh-server  openssh-client xinetd telnetd subversion nfs-kernel-server  nfs-common vim vim-common lm-sensors netcat automake autoconf libreadline-dev  libssl-dev libyaml-dev libffi-dev libtool unixodbc-dev  ubuntu-restricted-extras synaptic terminator default-jre default-jdk chromium-browser chromium-browser-l10n chromium-codecs-ffmpeg adobe-flashplugin shutter vim-gnome vim-gtk  vim-gui-common vim-snippets gimp swaks gir1.2-gtop-2.0 gir1.2-networkmanager-1.0  gir1.2-clutter-1.0 nodejs mysql-workbench mysql-workbench-data quodlibet redshift* filezilla* siege libsvn-java openvpn network-manager-openvpn network-manager-openvpn-gnome git-ftp  pgadmin3 rbenv ssh-askpass dia kdeconnect nvidia-cuda-dev nvidia-cuda-toolkit libssl-dev cmake cmake-curses-gui libjansson-dev clang libffi-dev nodejs yarn libgdbm-dev libncurses5-dev automake libtool bison libffi-dev linuxbrew-wrapper libmagic-dev  libjansson-dev clang psensor playonlinux libspice-client-gtk-3.0 software-properties-common gnome-tweak-tool gnome-shell-extensions gcc g++ make yarn wine1.8 tor torbrowser-launcher numix* sublime-text lxd lxd-client nfs-kernel-server syncthing docker git-lfs zsh python-pip tmux plank snapd apt-transport-https google-cloud-sdk kubectl google-cloud-sdk-app-engine-java google-cloud-sdk-app-engine-python  google-cloud-sdk-pubsub-emulator google-cloud-sdk-bigtable-emulator  google-cloud-sdk-datastore-emulator python-pip libpython-dev git-flow docker-ce virt-manager libvirt-bin ruby-libvirt qemu libvirt-bin virtinst virt-viewer ebtables dnsmasq libxslt-dev libxml2-dev libvirt-dev zlib1g-dev ruby-dev arc-theme spotify-client

ssh-keygen && fc-cache -fv && sudo sensors-detect && sudo service kmod start && sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

git config --global color.ui true
git config --global user.email "marcelo.cardenasc@gmail.com"
git config --global user.name "hackwish"

7. Otros paquetes (desde la web - DEB)

#GIT FLOW
wget --no-check-certificate -q  https://raw.githubusercontent.com/petervanderdoes/gitflow-avh/develop/contrib/gitflow-installer.sh && sudo bash gitflow-installer.sh install stable; rm gitflow-installer.sh

#MINIKUBE
curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.27.0/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/

#Atom Editor
wget -O atom-amd64.deb https://atom.io/download/deb
sudo gdebi atom-amd64.deb

#RUBY (VIA RBENV)
cd
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec $SHELL

git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
exec $SHELL

env | grep PATH
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

rbenv install 2.5.0
rbenv global 2.5.0
ruby -v

gem install bundler
rbenv rehash

#Rake
gem install rake
gem install bundler
gem install gemspec

#Brew
brew update
brew install python@2
brew install libmagic

#CAPISTRANO
gem install capistrano
gem install capistrano -v3.4.0
gem install capistrano-passenger

#PIP
pip install --upgrade pip
python -m pip install virtualenvwrapper --user

#AWS-CLI
python -m pip install awscli --upgrade --user


############################################

#Chrome
https://www.google.com/chrome/

#Teamviewer
https://www.teamviewer.com/es/download/linux.aspx

#Dropbox
https://www.dropbox.com/install?os=lnx

#MEGA
https://mega.nz/#sync!linux

#Oracle Virtualbox
https://www.virtualbox.org/wiki/Linux_Downloads

#Vagrant
https://www.vagrantup.com/downloads.html

#System76 Pop Theme
https://www.dropbox.com/s/qcss3ohm4k1xl20/Copy%20of%20PopTheme.zip?dl=0

#Visual Code Studio
https://code.visualstudio.com/Download

#SOAPUI
https://www.soapui.org/downloads/soapui.html

######################################################
######################################################
#vagrant
##Plugins
vagrant plugin install vagrant-mutate vagrant-libvirt vagrant-vbguest vagrant-puppet-install vocker  vagrant-lxc vagrant-list vagrant-hosts vagrant-hostmanager vagrant-exec vagrant-camera vagrant-cachier sahara vagrant-scp

vagrant global-status

error de modulos => sudo sed -i'' "s/Specification.all = nil/Specification.reset/" /usr/lib/ruby/vendor_ruby/vagrant/bundler.rb

#LXD
newgrp lxd
sudo lxd init

# PLUGINS SUBLIME #
Ir a la web de Package Control: https://packagecontrol.io/installation
(ctrl+alt+p install package)
DOCBLOCKR
EMMET
JSLINT
SUBLIME LINTER
SUBLIME CODEINTEL
BRACKET HIGHLIGHTER
SIDEBARENHANCEMENTS
SOLARIZED
ALIGNMENT
GIT
github tools
GITGUTTER
sublimegit
sublimegithub
gitignore
gitstatus
SVN
AllAutocomplete
LaravelBladeAutocomplete
SublimeREPL
ColorPicker
CanIUse
puppet
Sublinterpuppet
pretty yaml
codeigniter

# ZSH #
vim .zshrc
>>#ZSH_THEME="robbyrussell"
>>ZSH_THEME="random"

>>#plugins=(git)
>>
plugins=(
    asdf
    autopep8
    aws
    boot2docker
    bower
    brew
    bundler
    capistrano
    coffee
    colored-man-pages
    colorize
    command-not-found
    common-aliases
    compleat
    completion
    composer
    copybuffer
    copydir
    copyfile
    cp
    cpanm
    debian
    dircycle
    dirhistory
    dirpersist
    django
    docker
    docker-compose
    docker-machine
    dotenv
    encode64
    fasd
    Forklift
    geeknote
    gem
    git
    git_remote_branch
    git-extras
    git-flow
    git-flow-avh
    git-hubflow
    gitfast
    github
    gitignore
    go
    golang
    gradle
    grails
    history
    httpie
    jira
    kubectl
    laravel
    laravel4
    laravel5
    last-working-dir
    lol
    man
    marked2
    nanoc
    ng
    nmap
    node
    npm
    pass
    perl
    pip
    postgres
    profiles
    python
    rails
    rake
    rake-fast
    rbenv
    redis-cli
    repo
    rsync
    ruby
    rvm
    screen
    ssh-agent
    sublime
    sudo
    svn
    svn-fast-info
    systemd
    terminalapp
    terminitor
    themes
    thor
    torrent
    ubuntu
    urltools
    vagrant
    vi-mode
    vim-interaction
    virtualenv
    virtualenvwrapper
    vo-mode
    wd
    web-search
    wp-cli
    yarn
    yum
    zsh
    zsh_reload
    zsh-navigation-tools
)

 # You may need to manually set your language environment
 LANGUAGE=es_CL.UTF-8
 LANG=es_CL.UTF-8
 LC_CTYPE=es_CL.UTF-8
 LC_ALL=es_CL.UTF-8

##########################################
#TERMINALCOLOR
#18CAE6
#6FC3DF
##########################################

##############################################
##############################################

#Virtualbox
sudo apt-get install virtualbox virtualbox-dkms virtualbox-guest-additions-iso virtualbox-guest-dkms  virtualbox-qt libgsoap8 libvncserver1 virtualbox-guest-utils virtualbox-guest-x11

#vagrant
##Plugins
vagrant plugin install vagrant-mutate vagrant-libvirt vagrant-vbguest vagrant-puppet-install vocker  vagrant-lxc vagrant-list vagrant-hosts vagrant-hostmanager vagrant-exec vagrant-camera vagrant-cachier sahara vagrant-scp

vagrant global-status

error de modulos => sudo sed -i'' "s/Specification.all = nil/Specification.reset/" /usr/lib/ruby/vendor_ruby/vagrant/bundler.rb

#KVM
(verificar virt activa en : grep --color -e svm -e vmx /proc/cpuinfo )

adduser <suusuario> libvirt
adduser <suusuario> kvm

-En caso de error al iniciar la VM
editar /etc/libvirt/qemu.conf 
 y agregar:

user = "root" 
group = "root" 

systemctl restart libvirtd

#AWS Dragondisk
http://www.s3-client.com/download-s3-compatible-cloud-client.html

#NFS SERVER
apt-get install nfs-kernel-server 
vim /etc/exports
exportfs -a
sudo service nfs-kernel-server restart

#SYNCTHING
# Update and install syncthing:
sudo apt-get update
sudo apt-get install syncthing

#S3 DRAGONDISK
http://www.s3-client.com/download-s3-compatible-cloud-client.html



#DOCKER
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'

sudo apt-get update && apt-cache policy docker-engine

sudo apt-get install -y docker-engine

sudo systemctl status docker

##DOCKER MACHINE
 base=https://github.com/docker/machine/releases/download/v0.14.0 &&
  curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine &&
  sudo install /tmp/docker-machine /usr/local/bin/docker-machine



#GIT LFS

Ubuntu
Similar to Debian 7, Ubuntu 12 and similar Wheezy versions need to have a PPA repo installed to get git >= 1.8.2

sudo apt-get install software-properties-common to install add-apt-repository (or sudo apt-get install python-software-properties if you are on Ubuntu <= 12.04)

sudo add-apt-repository ppa:git-core/ppa

The curl script below calls apt-get update, if you aren't using it, don't forget to call apt-get update before installing git-lfs.
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install git-lfs
git lfs install


#ICONOS A LAS IZQUIERDA
gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize,maximize:'

gsettings set org.gnome.settings-daemon.plugins.xsettings overrides "{'Gtk/DecorationLayout':<'close,minimize,maximize:'>}"





